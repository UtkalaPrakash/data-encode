#include<stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include "manipulate_bits.h"


FILE *file_handler;
void hexdump(char *input, int address) {
	char *output = malloc(1026);
	char *temp = input;
	char symbol = *temp;
	int index = 0;
	int bytes;
	if ( feof(file_handler) ) {
		// Number of bytes actually read from fread in the last line
		bytes = nbytes(input, 17);
	}
	while (index < 16) {
	    if ( bytes  && bytes == index ) break;
	    if (index == 8) strcat(output, " ");
	    char hex_val[4] = "\0";
	    sprintf(hex_val, "%02x", (unsigned char) symbol);
	    strcat(hex_val, " ");

	    // Replace anything less than 0x20 and 0x7f with '.'
	    if ( hex_val[0] == '1' || hex_val[0] == '0' || (hex_val[0] == '7' && hex_val[1] == 'f') ) {
		*temp = '.';
	    }
	    // Replace anything greater than 7f with '~'
	    if ( hex_val[0] > '7' ) {
		*temp = '~';
	    }
	    strcat(output, hex_val);
	    temp++;
	    index++;
	    symbol = *temp;
	}
	// append '--' for the empty input characters at the end of the line
	while (index < 16) {
	    if (index == 8) strcat(output, " ");
	    strcat(output, "-- ");	
	    strcat(input, " ");
	    index++;
	}
	strcat(output, " \0");
	strcat(output, input);
	printf("%06x: %s\n",address, output);
	free(output);
}

void encode_base64(unsigned char b1, unsigned char b2, unsigned char b3, char *input) {
	int e1 = ascii_num((unsigned int) ((b1 & 252) >> 2));
	int e2 = ascii_num((unsigned int) (((b1 & 3) << 4) + (b2 >> 4)));
	int e3 = ascii_num((unsigned int) ((b2 & 15) << 2) + (b3 >> 6));
	int e4 = ascii_num((unsigned int) (b3 & 63));

	if ( feof(file_handler) ) {
		int bytes = nbytes(input, 4);
		if ( bytes == 0 ) {
		    // If no bytes exist print new line and end it
		    printf("\n");
		    exit(0);
		} else if ( bytes == 1 ) {
		    // If just 1 byte exist last 2 characters are '='
		    e3 = 61;
		    e4 = 61;
		} else if ( bytes == 2 ) {
		    // If 2 bytes exists last charater is '='
		    e4 = 61;
		} else {}
	}

	printf("%c%c%c%c", e1, e2, e3, e4);
}

void decode_base64(unsigned char e1, unsigned char e2, unsigned char e3, unsigned char e4) {
	if ( feof(file_handler) ) exit(0);
	int b1, b2, b3;
	b1 = (unsigned int) ((base64(e1) & 63) << 2) + (unsigned int) ((base64(e2) & 48) >> 4);
	b2 = (unsigned int) ((base64(e2) & 15) << 4) + (unsigned int) ((base64(e3) & 60) >> 2);
	b3 = (unsigned int) ((base64(e3) & 3) << 6) + (unsigned int) (base64(e4) & 63);
	if (!base64_char(e1) || !base64_char(e2) || !base64_char(e3) || !base64_char(e4)) {
 		fprintf(stderr, "Content is not in encoded base 64 format \n");
		exit(0);
 	}

	if (e3 == '=' && e4 == '=') {
		// If last 2 characters are '=' only b1 exists
		printf("%c", b1);
	} else if ( e4 == '=' ) {
		// If last character is '=' b3 doesn't exist
		printf("%c%c", b1, b2);
	} else {
		// If there is no '=' all characters exist
		printf("%c%c%c", b1, b2, b3);
	}
}

int main(int argc, char **argv) {
	char file_name[100];
	char option[11];
	if ( argc != 2 && argc != 3 ) {
		fprintf(stderr, "Improper usage\nUsage: ./hw1 [hexdump|enc-base64|dec-base64] [file]\n");
        	exit(0);
	}

	// Using my code from warmup assignments in CSCI 402
	if ( strcmp(*(argv+1), "hexdump") != 0 && strcmp(*(argv+1), "enc-base64") != 0 && strcmp(*(argv+1), "dec-base64") != 0 ) {
	    fprintf(stderr, "Malformed command\nUsage: ./hw1 [hexdump|enc-base64|dec-base64] [file]\n");
	    exit(0);
	}
	if ( argc == 2 ) {
		strcpy(option, *(argv+1));
		file_handler = stdin;
	} else {
		strcpy(file_name, *(argv+2));
		strcpy(option, *(argv+1));
		if ( access(file_name, F_OK) == -1 ) {
        	    fprintf(stderr, "Input file %s doesn't exist\n", file_name);
        	    exit(0);
        	}

        	struct stat file_check;
        	stat(file_name, &file_check);
        	if ( S_ISDIR(file_check.st_mode) ) {
        	    fprintf(stderr, "Input file %s is a directory\n", file_name);
        	    exit(0);
        	}

        	if ( access(file_name, R_OK) == -1 ) {
        	    fprintf(stderr, "Cannot read input file %s, permission denied\n", file_name);
        	    exit(0);
        	}

        	file_handler = fopen(file_name, "r");
        	if ( file_handler == NULL) {
        	    fprintf(stderr, "Input file %s cannot be open\n", file_name);
        	    exit(0);
        	}
	}

	int address = 0;
	if (strcmp(option, "hexdump") == 0) {
		do {
			char read_buffer[17] = "";
			fread(read_buffer, 16, 1, file_handler);
			hexdump(read_buffer, address);
			// Every line has 16 bytes so add 16 for address
			address += 16;
		} while ( !feof(file_handler) );
	} else if (strcmp(option, "enc-base64") == 0) {
		do {
			address++;
			char read_buffer[4] = "";
			fread(read_buffer, 3, 1, file_handler);
			encode_base64(read_buffer[0], read_buffer[1], read_buffer[2], read_buffer);
			if ( address == 16 ) {
				// Add new line after 64 bytes
				printf("\n");
				address = 0;
			}
		} while ( !feof(file_handler) );
	} else {
		do {
			address++;
                        char read_buffer[5] = "";
                        fread(read_buffer, 4, 1, file_handler);
			// Replace new line character with next character
			while ( read_buffer[0] == '\n' || read_buffer[1] == '\n' || read_buffer[2] == '\n' || read_buffer[3] == '\n' ) {
			    if ( read_buffer[0] == '\n' ) {
				char b;
				fread(&b, 1, 1, file_handler);
				read_buffer[0] = read_buffer[1];
				read_buffer[1] = read_buffer[2];
				read_buffer[2] = read_buffer[3];
				read_buffer[3] = b;
			    } else if ( read_buffer[1] == '\n' ) {
				char b;
				fread(&b, 1, 1, file_handler);
				read_buffer[1] = read_buffer[2];
				read_buffer[2] = read_buffer[3];
				read_buffer[3] = b;
			    } else if ( read_buffer[2] == '\n' ) {
				char b;
				fread(&b, 1, 1, file_handler);
				read_buffer[2] = read_buffer[3];
				read_buffer[3] = b;
			    } else {
				char b;
				fread(&b, 1, 1, file_handler);
				read_buffer[3] = b;
			    }
			}
                        decode_base64(read_buffer[0], read_buffer[1], read_buffer[2], read_buffer[3]);
		} while ( !feof(file_handler) );
	}
	if (strcmp(option, "enc-base64") == 0) printf("\n");
	fclose(file_handler);
	return 0;
}
