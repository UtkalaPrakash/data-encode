hw1: hw1.o
	gcc -o hw1 -g hw1.o

hw1.o: hw1.c manipulate_bits.h
	gcc -g -c -Wall hw1.c

clean:
	rm -f *.o hw1
